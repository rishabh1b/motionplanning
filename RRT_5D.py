# -*- coding: utf-8 -*-
"""
Created on Sat Apr  8 17:45:56 2017

@author: rishabh
"""

import math
import sys
import numpy as np
import matplotlib.pyplot as plt

class RRTTree:
    def __init__(self, start_vert, goal_vert, goal_radius):
        self.root = start_vert
        self.goal = goal_vert
        self.goalradius = goal_radius
        self.vertices = []
        self.edges = {}
        self.goalradius = goal_radius
        
        self.addnode(start_vert)
        self.trajectory_map = {}
        self.time_map = {}
        self.addedge(start_vert, None, None, None)
        self.k_theta = 1
        self.k_v_om = 1
        
    def addnode(self, vert):
        self.vertices.append(vert)
        
    def addedge(self, vert, parent, traj_points, traj_props):
        self.edges[vert] = parent
        #self.trajectory_map[parent] = np.poly1d(coeffs)
        self.trajectory_map[(parent, vert)] = traj_points
        self.time_map[(parent, vert)] = traj_props
    
    def findnearestnode(self, point):
        min_pos = 0
        min_dist = self.__findminimumxy__((point[0], point[1]), 0) + self.k_theta * self.__findminimumtheta__(point[2], 0) + \
                   self.k_v_om * self.__findminimumvom__((point[3], point[4]), 0)
        for i in range(1, len(self.vertices)):
            dist = self.__findminimumxy__((point[0], point[1]), i) + self.k_theta * self.__findminimumtheta__(point[2], i) + \
                   self.k_v_om * self.__findminimumvom__((point[3], point[4]), i)
            if  dist < min_dist:
                min_pos = i
                min_dist = dist
                
        return self.vertices[min_pos]
        
    def __findminimumxy__(self, point, ind):
        return math.sqrt((self.vertices[ind][1] - point[1])**2 + (self.vertices[ind][0] - point[0])**2)
        
    def __findminimumtheta__(self, theta, ind):
        return min(abs(theta - self.vertices[ind][2]), (2 * 3.146 - abs(theta - self.vertices[ind][2])))
        
    def __findminimumvom__(self, vel, ind):
        return math.sqrt((self.vertices[ind][3] - vel[0])**2 + (self.vertices[ind][4] - vel[1])**2)

class ConfigSpace:   
    def __init__(self, start_vert, goal_vert, workspace_obstacles, goal_radius = 3):
        self.start = start_vert
        self.goal = goal_vert
        self.rrt = RRTTree(start_vert, goal_vert, goal_radius)
        self.define_config_space_size(start_vert, goal_vert)
        self.goalfound = False
        # Need a Function to map the obstacles from workspace to configuration space
        self.obstacles = workspace_obstacles
        
        # Final Result
        self.path_to_goal = []
        
    def define_config_space_size(self, start, goal, padding = 5):
        self.lower_lim_x = 0
        self.upper_lim_x = 100
        self.lower_lim_y = 0
        self.upper_lim_y = 100
        self.v_min = -5
        self.v_max = 5
        self.om_min = -1.57
        self.om_max = 1.57
        self.theta_min = 0
        self.theta_max = 6.28
        
    def sample(self):
        x_sample = (self.upper_lim_x - self.lower_lim_x) * np.random.random_sample() + self.lower_lim_x
        y_sample = (self.upper_lim_y - self.lower_lim_y) * np.random.random_sample() + self.lower_lim_y
        theta_sample = (self.theta_max - self.theta_min) * np.random.random_sample() + self.theta_min
        v_sample = (self.v_max - self.v_min) * np.random.random_sample() + self.v_min  
        omega_sample = (self.om_max - self.om_min) * np.random.random_sample() + self.om_min
        return x_sample, y_sample, theta_sample, v_sample, omega_sample

    # Modify the sample based on current heading and goto position to obtain a feasible trajectory
    def modify_sample(self, sample, near_sample):
        mod_sample = [sample[2], sample[3], sample[4]]
        theta = near_sample[2]
        dir = math.atan2(sample[1] - near_sample[1], sample[0] - near_sample[0])
        if abs(near_sample[2] - abs(dir)) > 1.57:
            mod_sample[1] = -(self.v_max) * np.random.random_sample()
        else:
            mod_sample[1] = (self.v_max) * np.random.random_sample()
        vec_1 = [math.cos(theta), math.sin(theta), 1]
        vec_2 = [sample[0] - near_sample[0], sample[1] - near_sample[1], 1]
        vec = np.cross(vec_1, vec_2)
        if (np.sign(vec[2]) > 0 and np.sign(mod_sample[1]) > 0) or (np.sign(vec[2]) < 0 and np.sign(mod_sample[1]) < 0):
            mod_sample[0] = 1.57 * np.random.random_sample() + near_sample[2]
            mod_sample[2] = (0.1) * np.random.random_sample()
        else:
            mod_sample[0] = -1.57 * np.random.random_sample() + near_sample[2]
            mod_sample[2] = -(0.1) * np.random.random_sample()
        if mod_sample[0] > 6.28:
            mod_sample[0] = 6.28 - mod_sample[0]
        if mod_sample[0] < 0:
            mod_sample[0] = mod_sample[0] + 6.28

        return (sample[0], sample[1], mod_sample[0], mod_sample[1], mod_sample[2])

    def __is_goal_region__(self, vert):
        return math.sqrt(abs(self.rrt.goal[1] - vert[1])**2 + abs(self.rrt.goal[0] - vert[0])**2) < self.rrt.goalradius
        
    def insertnode(self, vert):
        self.rrt.addnode(vert)
        if self.__is_goal_region__(vert):
            self.goalfound = True
            
    def insertedge(self, vert, parent, traj_points, traj_props):
        self.rrt.addedge(vert, parent, traj_points, traj_props)
        
    def getnearestnode(self, point):
        return self.rrt.findnearestnode(point)

    def isValidSample(self, point):
        return (point[0] >= self.lower_lim_x and point[0] < self.upper_lim_x and point[1] >= self.lower_lim_y and point[1] < self.upper_lim_y)
            
        
class WorkSpace:   
    def __init__(self, obstacles):
        self.obstacles = obstacles
        self.delta = 0.25
        
    def check_sample(self, point):
        for i in range(len(self.obstacles)):
            if math.sqrt(abs(self.obstacles[i][1] - point[1])**2 + abs(self.obstacles[i][0] - point[0])**2) <= self.obstacles[i][2]:
                return False
                
        return True

    def check_collision(self, trajs):
        X = trajs[0]
        Y = trajs[1]
        for i in range(len(X)):
            if not self.check_sample((X[i],Y[i])):
                return False
        return True
            
def RRT(start, goal, goal_radius, obstacles, epsilon = 1):
    configspace = ConfigSpace(start, goal, obstacles, goal_radius)
    # Set the Environment
    my_dpi = 96
    plt.figure(1, figsize=(800/my_dpi, 800/my_dpi), dpi=my_dpi)
    plt.cla()
    plt.xlim ( -10, 110 )
    plt.ylim ( -10, 110 )
    plt.gca().set_aspect('equal', adjustable='box')
    plt.xlabel('X ')
    plt.ylabel('Y ')
    ax = plt.axes()
    
    # Plot the Start and End Point
    plt.plot([start[0]], [start[1]], 'bo', markersize=10)
    plt.plot([goal[0]], [goal[1]], 'go', markersize=10)

    # Plot the goal region
    goal_circle = plt.Circle((goal[0], goal[1]), goal_radius, facecolor='none', linestyle='dashdot', edgecolor='g',
                             linewidth=2)
    ax.add_artist(goal_circle)
        
    # Plot the Obstacles
    for i in range(len(configspace.obstacles)):
        circle = plt.Circle((configspace.obstacles[i][0], configspace.obstacles[i][1]),configspace.obstacles[i][2], color='m')
        ax.add_artist(circle)

    workspace = WorkSpace(obstacles)
    new_sample = ()
    
    # Some Parameters
    n_s = 1000 # Number of time steps to complete a trajectory
    a_max = 2
    al_max = 1.57

    # For debugging
    #f = open('Debug\H4.txt', 'w')
    #### The Main RRT Loop
    while not configspace.goalfound:
        curr_t = 0
        lin_acc = a_max + 1
        ang_acc = al_max + 1
        nearest_node = ()
        while abs(lin_acc) > a_max or abs(ang_acc) > al_max:
            new_sample = configspace.sample()
            while not workspace.check_sample((new_sample[0], new_sample[1])):
                new_sample = configspace.sample()
            nearest_node = configspace.getnearestnode(new_sample)
            x_n = new_sample[0]
            y_n = new_sample[1]
            theta_n = new_sample[2]
            v_n = new_sample[3]
            om_n = new_sample[4]
            dist = math.sqrt((nearest_node[1] - new_sample[1]) ** 2 + (nearest_node[0] - new_sample[0]) ** 2)
            if dist > epsilon:
                x_n = nearest_node[0] + (new_sample[0] - nearest_node[0]) * epsilon / dist
                y_n = nearest_node[1] + (new_sample[1] - nearest_node[1]) * epsilon / dist

            new_sample = (x_n, y_n, theta_n, v_n ,om_n)
            new_sample = configspace.modify_sample(new_sample, nearest_node)
            dist = math.sqrt((nearest_node[1] - new_sample[1]) ** 2 + (nearest_node[0] - new_sample[0]) ** 2)

            #Get an estimate of time required for the trajectory
            curr_t = abs(2 * dist / (new_sample[3] + nearest_node[3]))
            lin_acc = (new_sample[3] - nearest_node[3]) / (curr_t)
            ang_acc = (new_sample[4] - nearest_node[4]) / (curr_t)

        # Forward Simulation
        t = 0
        t_step = curr_t / n_s
        omega_1 = nearest_node[4]
        vel_1 = nearest_node[3]
        theta_1 = nearest_node[2]
        x_1 = nearest_node[0]
        y_1 = nearest_node[1]
        x_t = [x_1]
        y_t = [y_1]
        theta = 0
        omega = 0
        vel = 0
        while t <= n_s * t_step :
            omega = omega_1 + ang_acc * t_step
            theta = theta_1 + omega * t_step
            if theta > 2 * 3.146:
                theta = theta -  2 * 3.146
            if theta < 0:
                theta = theta + 2 * 3.146
            vel = vel_1 + lin_acc * t_step
            del_x = (vel) * math.cos(theta) * t_step
            del_y = vel * math.sin(theta) * t_step
            x_t.append(x_1 + del_x)
            y_t.append(y_1 + del_y)
            x_1 = x_1 + del_x
            y_1 = y_1 + del_y
            theta_1 = theta
            vel_1 = vel
            omega_1 = omega
            t = t + t_step

        new_sample = (x_1, y_1, theta, vel, omega)
        #x_traj = np.linspace(x_t[0], x_t[-1], 0.25 * n_s)
        #y_traj = np.linspace(y_t[0], y_t[-1], 0.25 * n_s)
        if workspace.check_collision((x_t, y_t)) and configspace.isValidSample(new_sample):
            configspace.insertnode(new_sample)
            configspace.insertedge(new_sample, nearest_node, (x_t,y_t), (curr_t, lin_acc, ang_acc))
    
     # Extract the path from the RRT Tree            
    edges = configspace.rrt.edges
    path = [new_sample]
    parent = edges[new_sample]
    while parent is not None:
        path.append(parent)
        parent = edges[parent]
    
    path_to_goal = list(reversed(path))
    
    # Plot the Path to goal
    f = open('Solution.txt', 'w')
    t_curr = 0
    pos_curr = start
    for i in range(len(path_to_goal) - 1):
        path_x = configspace.rrt.trajectory_map[(path_to_goal[i], path_to_goal[i+1])][0]
        path_y = configspace.rrt.trajectory_map[(path_to_goal[i], path_to_goal[i+1])][1]
        plt.plot(path_x, path_y, linewidth=3, color='r')
        traj_props = configspace.rrt.time_map[(path_to_goal[i], path_to_goal[i+1])]
        f.write(str(t_curr) + ',' + str(pos_curr[0]) + ','  + str(pos_curr[1]) + ',' + str(pos_curr[2]) + ',' +  str(traj_props[1]) + ',' + str(traj_props[2]) + '\n')
        t_curr = t_curr + traj_props[0]
        pos_curr = path_to_goal[i+1]

    f.write(str(t_curr) + ',' + str(pos_curr[0]) + ',' + str(pos_curr[1]) + ',' + str(pos_curr[2]) + ',' + str(0) + ',' + str(0) + '\n')
    f.close()
    plt.show()

# Main function to read the file and call RRT
def main (args):
    inpfileobs = open("Data/obstaclesH4.txt", "r")
    obstacles = []
    for line in inpfileobs:
        curr_obs = line.split(',')
        curr_obs_x = float(curr_obs[0])
        curr_obs_y = float(curr_obs[1])
        curr_obs_r = float(curr_obs[2])
        obstacles.append((curr_obs_x, curr_obs_y, curr_obs_r))
    inpfileobs.close()
    
    start = (10,90,3*3.146/2,0,0)
    goal = (100,90,0,0,0)
    goal_radius = 10
    epsilon = 5
    
    RRT(start, goal, goal_radius, obstacles, epsilon)
    
if __name__ == '__main__':
    main(sys.argv)
            
        
        
    